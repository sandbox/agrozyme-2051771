<?php

/**
 * Gets the last caller from a backtrace.
 *
 * @param $backtrace
 *   A standard PHP backtrace.
 *
 * @return
 *   An associative array with keys 'file', 'line' and 'function'.
 */
function _drupal_get_last_caller($backtrace) {
	// Errors that occur inside PHP internal functions do not generate
	// information about file and line. Ignore black listed functions.
	$blacklist = array('debug', '_drupal_error_handler', '_drupal_exception_handler');
	while (($backtrace && !isset($backtrace[0]['line'])) ||
	(isset($backtrace[1]['function']) && in_array($backtrace[1]['function'], $blacklist))) {
		array_shift($backtrace);
	}

	// The first trace is the call itself.
	// It gives us the line and the file of the last call.
	$call = $backtrace[0];

	// The second call give us the function where the call originated.
	if (isset($backtrace[1])) {
		if (isset($backtrace[1]['class'])) {
			$call['function'] = $backtrace[1]['class'] . $backtrace[1]['type'] . $backtrace[1]['function'] . '()';
		} else {
			$call['function'] = $backtrace[1]['function'] . '()';
		}
	} else {
		$call['function'] = 'main()';
	}
	return $call;
}
