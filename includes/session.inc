<?php

function drupal_save_session($status = NULL) {
	// PHP session ID, session, and cookie handling happens in the global scope.
	// This value has to persist across calls to drupal_static_reset(), since a
	// potentially wrong or disallowed session would be written otherwise.
	static $save_session = TRUE;
	if (isset($status)) {
		$save_session = $status;
	}
	return $save_session;
}

function drupal_session_started($set = NULL) {
	static $session_started = FALSE;
	if (isset($set)) {
		$session_started = $set;
	}
	return $session_started && session_id();
}

function drupal_session_start() {
	// Command line clients do not support cookies nor sessions.
	if (!drupal_session_started() && !drupal_is_cli()) {
		// Save current session data before starting it, as PHP will destroy it.
		$session_data = isset($_SESSION) ? $_SESSION : NULL;

		session_start();
		drupal_session_started(TRUE);

		// Restore session data.
		if (!empty($session_data)) {
			$_SESSION += $session_data;
		}
	}
}

function drupal_session_commit() {
	global $user, $is_https;

	if (!drupal_save_session()) {
		// We don't have anything to do if we are not allowed to save the session.
		return;
	}

	if (empty($user->uid) && empty($_SESSION)) {
		// There is no session data to store, destroy the session if it was
		// previously started.
		if (drupal_session_started()) {
			session_destroy();
		}
	} else {
		// There is session data to store. Start the session if it is not already
		// started.
		if (!drupal_session_started()) {
			drupal_session_start();
			if ($is_https && variable_get('https', FALSE)) {
				$insecure_session_name = substr(session_name(), 1);
				$params = session_get_cookie_params();
				$expire = $params['lifetime'] ? REQUEST_TIME + $params['lifetime'] : 0;
				setcookie($insecure_session_name, $_COOKIE[$insecure_session_name], $expire, $params['path'], $params['domain'], FALSE, $params['httponly']);
			}
		}
		// Write the session data.
		session_write_close();
	}
}

