<?php

function drupal_exit($destination = NULL) {
	if (drupal_get_bootstrap_phase() == DRUPAL_BOOTSTRAP_FULL) {
		if (!defined('MAINTENANCE_MODE') || MAINTENANCE_MODE != 'update') {
			module_invoke_all('exit', $destination);
		}
		drupal_session_commit();
	}
	exit;
}

function debug($data, $label = NULL, $print_r = FALSE) {
	// Print $data contents to string.
	$string = check_plain($print_r ? print_r($data, TRUE) : var_export($data, TRUE));

	// Display values with pre-formatting to increase readability.
	$string = '<pre>' . $string . '</pre>';

	trigger_error(trim($label ? "$label: $string" : $string));
}

function &drupal_array_get_nested_value(array &$array, array $parents, &$key_exists = NULL) {
	$ref = &$array;
	foreach ($parents as $parent) {
		if (is_array($ref) && array_key_exists($parent, $ref)) {
			$ref = &$ref[$parent];
		} else {
			$key_exists = FALSE;
			$null = NULL;
			return $null;
		}
	}
	$key_exists = TRUE;
	return $ref;
}

