<?php

function &drupal_static($name, $default_value = NULL, $reset = FALSE) {
	static $data = array(), $default = array();
	// First check if dealing with a previously defined static variable.
	if (isset($data[$name]) || array_key_exists($name, $data)) {
		// Non-NULL $name and both $data[$name] and $default[$name] statics exist.
		if ($reset) {
			// Reset pre-existing static variable to its default value.
			$data[$name] = $default[$name];
		}
		return $data[$name];
	}
	// Neither $data[$name] nor $default[$name] static variables exist.
	if (isset($name)) {
		if ($reset) {
			// Reset was called before a default is set and yet a variable must be
			// returned.
			return $data;
		}
		// First call with new non-NULL $name. Initialize a new static variable.
		$default[$name] = $data[$name] = $default_value;
		return $data[$name];
	}
	// Reset all: ($name == NULL). This needs to be done one at a time so that
	// references returned by earlier invocations of drupal_static() also get
	// reset.
	foreach ($default as $name => $value) {
		$data[$name] = $value;
	}
	// As the function returns a reference, the return should always be a
	// variable.
	return $data;
}

function drupal_static_reset($name = NULL) {
	drupal_static($name, NULL, TRUE);
}

function format_string($string, array $args = array()) {
	// Transform arguments before inserting them.
	foreach ($args as $key => $value) {
		switch ($key[0]) {
			case '@':
				// Escaped only.
				$args[$key] = check_plain($value);
				break;

			case '%':
			default:
				// Escaped and placeholder.
				$args[$key] = drupal_placeholder($value);
				break;

			case '!':
			// Pass-through.
		}
	}
	return strtr($string, $args);
}

function drupal_is_cli() {
	return (!isset($_SERVER['SERVER_SOFTWARE']) && (php_sapi_name() == 'cli' || (is_numeric($_SERVER['argc']) && $_SERVER['argc'] > 0)));
}

function drupal_get_bootstrap_phase() {
	return drupal_bootstrap(NULL);
}

